console.log("Script Loaded - YAAY!");

var buttonOnClick = function() {
    const pi = Math.PI;
    let dmeter = document.getElementById('diameter').value;
    let pCost = document.getElementById('cost').value;

    const outputArea = document.getElementById('outputResults');

    while (outputArea.firstChild)
        outputArea.removeChild(outputArea.firstChild);

    if (isNaN(dmeter) | dmeter == "") {
        let ErrorMessage = document.createTextNode("Please Enter values.");
        outputArea.appendChild(ErrorMessage);
    } else {

        let pizzaArea = pi * (Math.pow((dmeter / 2), 2));
        let cPerSqInch = pCost / pizzaArea;

        let resText = document.createTextNode(`The Pizza Area for a ${dmeter} inch pizza is: ${pizzaArea}
  and costs $${cPerSqInch} per square inch!`);

        outputArea.appendChild(resText);

        console.log(`The Pizza Area for a ${dmeter} inch pizza is: ${pizzaArea}
      and costs: ${cPerSqInch} per square inch!`);
    }
    //END OF FUNCTION buttonOnClick
};

var createPerson = function() {

    let person = {
        pFirstName: document.getElementById('firstName').value,
        pLastName: document.getElementById('lastName').value,
        pStreet: document.getElementById('street').value,
        pCity: document.getElementById('city').value,
        pState: document.getElementById('state').value,
        pZipCode: document.getElementById('zipCode').value
    };

    let fullName = person.pFirstName + ' ' + person.pLastName;

    const personAddress = document.getElementById('outputPersonAddress');

    while (personAddress.firstChild)
        personAddress.removeChild(personAddress.firstChild);

    let address = document.createTextNode(`${fullName}
${person.pStreet}
${person.pCity}, ${person.pState} ${person.pZipCode}`);

    personAddress.appendChild(address);
    console.log(address);

    //END OF FUNCTION createPerson
};

var dealCards = function(x=3) {
    let minVal = 1;
    let maxVal = 13;
    let dealtHand = [];
    let numberOfCards = document.getElementById('cards').value;

    if (isNaN(numberOfCards) | numberOfCards == "") {
        numberOfCards = x
    }
    ;
    for (let idx = 0; idx < numberOfCards; idx++) {
        dealtHand.push(Math.floor(Math.random() * (maxVal - minVal + 1)) + minVal);
    }
    ;let maxCard = Math.max(...dealtHand);

    console.log(dealtHand);
    console.log(maxCard);

    const dealtCards = document.getElementById('outputDealtCards');

    while (dealtCards.firstChild)
        dealtCards.removeChild(dealtCards.firstChild);

    dealtCards.appendChild(document.createTextNode(`Cards dealt: ${dealtHand}
Max Card Drawn: ${maxCard}`));

    //END OF FUNCTION dealCards
};

var getMidDate = function() {

    let startDate = document.getElementById('starter').value;
    let endDate = document.getElementById('end-date').value;

    const calculatedMidDate = document.getElementById('outputMiddleDate');
    while (calculatedMidDate.firstChild)
        calculatedMidDate.removeChild(calculatedMidDate.firstChild);

    if(startDate === '' | endDate === '') {
        calculatedMidDate.appendChild(document.createTextNode(`One of the date parameters is empty.`));
    }
    else {
      let year = startDate.substring(0,startDate.indexOf('-',1));
      let mon = startDate.substring(startDate.indexOf('-',1)+1,startDate.lastIndexOf('-'));
      let day = startDate.substring(startDate.lastIndexOf('-')+1,startDate.length);
      let newStart = new Date(year,parseInt(mon)-1,day);

      year = endDate.substring(0,endDate.indexOf('-',1));
      mon = endDate.substring(endDate.indexOf('-',1)+1,endDate.lastIndexOf('-'));
      day = endDate.substring(endDate.lastIndexOf('-')+1,endDate.length);
      let newEnd = new Date(year,parseInt(mon)-1,day);

      let middleDate = new Date((newStart.getTime()+newEnd.getTime())/2);
      calculatedMidDate.appendChild(document.createTextNode(`Calculated Middle Date:
--> ${middleDate}`));
    }

  //END OF FUNCTION getMidDate
};

var getFirstName = function(){
  let address = document.getElementById('formattedAddress').value;
  retFirstName = address.slice(0,address.indexOf(' ',1));

  const retrievedName = document.getElementById('outputFormattedAddress');
  while (retrievedName.firstChild)
      retrievedName.removeChild(retrievedName.firstChild);
  retrievedName.appendChild(document.createTextNode(`First Name from Address
--> ${retFirstName}`))

  //END OF FUNCTION getFirstname
};
